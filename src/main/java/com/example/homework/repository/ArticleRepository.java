package com.example.homework.repository;

import com.example.homework.model.Article;

import java.util.List;


public interface ArticleRepository {
    void save(Article article);

    List<Article> findAll();

    void initArticle(List<Article> articleList);

    void deleteById(String id);

    void updateById(Article article,String id);
}
