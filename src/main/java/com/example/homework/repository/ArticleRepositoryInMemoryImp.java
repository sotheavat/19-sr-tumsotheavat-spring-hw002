package com.example.homework.repository;

import com.example.homework.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ArticleRepositoryInMemoryImp implements ArticleRepository {
    List<Article> articleList = new ArrayList<>();

    @Override
    public void save(Article article) {
        articleList.add(article);
    }

    @Override
    public List<Article> findAll() {
        return articleList;
    }

    @Override
    public void initArticle(List<Article> articleList) {
        this.articleList.addAll(articleList);
    }

    @Override
    public void deleteById(String id) {
        articleList = articleList.stream().filter(article -> !(String.valueOf(article.getId()).equalsIgnoreCase(id))).collect(Collectors.toList());
    }

    @Override
    public void updateById(Article article,String id) {
        deleteById(id);
        articleList.add(new Article(Integer.parseInt(id),article.getTitle(),article.getDescription(),article.getPicture()));
    }
}
