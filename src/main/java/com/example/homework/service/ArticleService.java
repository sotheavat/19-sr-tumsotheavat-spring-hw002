package com.example.homework.service;

import com.example.homework.model.Article;
import com.example.homework.repository.ArticleRepositoryInMemoryImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {
    private final ArticleRepositoryInMemoryImp articleRepository;
    @Autowired
    public ArticleService(ArticleRepositoryInMemoryImp articleRepository) {
        this.articleRepository = articleRepository;
    }
    public void saveArticle(Article article){
        articleRepository.save(article);
    }

    public List<Article> getArticleList() {
        return articleRepository.findAll();
    }

    public void initArticle(List<Article> articleList){
        articleRepository.initArticle(articleList);
    }

    public void deleteArticleById(String id){
        articleRepository.deleteById(id);
    }

    public void updateArticle(Article updatedArticle) {
        articleRepository.updateById(updatedArticle,String.valueOf(updatedArticle.getId()));
    }
}
